using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Runtime.InteropServices;

namespace SeleniumCoreDemo
{
    public class Tests
    {
        //Hooks in NUnit
        [SetUp]
        public void Setup()
        {
           
        }

        [Test]
        public void Test1()
        {
            //Add chrome options 
            ChromeOptions options = new ChromeOptions();
           options.AddArguments("--headless", "--window-size=1920,1200", "--no-sandbox", "--disable-gpu", "--disable-extensions");
            

            //Browser driver
            IWebDriver webDriver = new ChromeDriver(options);
           //IWebDriver webDriver = new ChromeDriver();


            //Navigate to URL
            webDriver.Navigate().GoToUrl("http://eaapp.somee.com/");


            //Identify Login
            IWebElement lnklogin = webDriver.FindElement(By.LinkText("Login")) ;

            //operation (Act)
            lnklogin.Click();

            //UserName

            var txtUserName = webDriver.FindElement(By.Name("UserName"));

            //Assertion
            Assert.That(txtUserName.Displayed, Is.True);

            txtUserName.SendKeys("admin");
            webDriver.FindElement(By.Name("Password")).SendKeys("password");
            webDriver.FindElement(By.XPath("//input[@value='Log in']")).Submit();
            var lnkEmployeeDetails = webDriver.FindElement(By.LinkText("Employee Details"));
            Assert.That(lnkEmployeeDetails.Displayed, Is.True);
            webDriver.Quit();

        }


        [Test]
        public void Test2()
        {
            //Add chrome options 
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--headless", "--window-size=1920,1200", "--no-sandbox");


            //Browser driver
            IWebDriver webDriver = new ChromeDriver(options);
            //IWebDriver webDriver = new ChromeDriver();


            //Navigate to URL
            webDriver.Navigate().GoToUrl("http://eaapp.somee.com/");


            //Identify Login
            IWebElement lnklogin = webDriver.FindElement(By.LinkText("Login"));

            //operation (Act)
            lnklogin.Click();

            //UserName

            var txtUserName = webDriver.FindElement(By.Name("UserName"));

            //Assertion
            Assert.That(txtUserName.Displayed, Is.True);

            txtUserName.SendKeys("admin");
            webDriver.FindElement(By.Name("Password")).SendKeys("password");
            webDriver.FindElement(By.XPath("//input[@value='Log in']")).Submit();


            webDriver.FindElement(By.LinkText("Employee List")).Click();
            webDriver.FindElement(By.XPath("//a[normalize-space()='Create New']")).Click();
            webDriver.FindElement(By.Id("Name")).SendKeys("DD34");
            webDriver.FindElement(By.Id("Salary")).SendKeys("1000");
            webDriver.FindElement(By.Id("DurationWorked")).SendKeys("20");
            webDriver.FindElement(By.Id("Grade")).SendKeys("1");
            webDriver.FindElement(By.Id("Email")).SendKeys("test@testgmail.com");
            webDriver.FindElement(By.XPath("//input[@value='Create']")).Click();
            var Searchname = webDriver.FindElement(By.XPath("//input[@placeholder='Search Name']"));
            Assert.That(Searchname.Displayed, Is.True);
            webDriver.FindElement(By.XPath("//input[@placeholder='Search Name']")).SendKeys("DD34");
            webDriver.FindElement(By.XPath("//input[@value='Search']")).Click();
            var user = webDriver.FindElement(By.XPath("//td[normalize-space()='DD34']"));
            Assert.That(user.Displayed, Is.True);
            webDriver.Quit();

        }
    }
}