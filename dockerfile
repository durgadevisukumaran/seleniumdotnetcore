# Selenium provided base image that contains selenium and the chrome browser
FROM selenium/standalone-chrome 

 # Add the Microsoft package signing key to your list of trusted keys
 RUN sudo wget https://packages.microsoft.com/config/debian/10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb \
 && sudo dpkg -i packages-microsoft-prod.deb \
 && sudo rm packages-microsoft-prod.deb

 # Install SDK
 RUN sudo apt-get update && sudo apt-get install -y \
 sudo apt-transport-https && \
 sudo apt-get update && sudo apt-get install -y \
 dotnet-sdk-3.1
 
 # Install Runtime
 RUN sudo apt-get update && sudo apt-get install -y \
 sudo apt-transport-https && \
 sudo apt-get update && sudo apt-get install -y \
 Aspnetcore-runtime-3.1